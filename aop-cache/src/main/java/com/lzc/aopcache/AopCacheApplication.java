package com.lzc.aopcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AopCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(AopCacheApplication.class, args);
    }

}
