package com.lzc.aopcache.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 自定义缓存注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LzcCache {

    String cacheName() default "";

    String key() default "";

    long timeOut() default 0;

    TimeUnit timeUnit() default TimeUnit.HOURS;
}
