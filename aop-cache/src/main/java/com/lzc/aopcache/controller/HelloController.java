package com.lzc.aopcache.controller;

import com.lzc.aopcache.annotation.LzcCache;
import com.lzc.aopcache.annotation.LzcCacheEvict;
import com.lzc.aopcache.dataobject.User;
import com.lzc.aopcache.utils.ResultVOUtil;
import com.lzc.aopcache.vo.ResultVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class HelloController {

    @GetMapping("/cache")
    @LzcCache(cacheName = "user", key = "#user.username", timeOut = 10, timeUnit = TimeUnit.MINUTES)
    public ResultVO cahce(User user) {
        System.out.println("进来了");
        return ResultVOUtil.success(user);
    }

    @GetMapping("/cache1")
    @LzcCacheEvict(cacheName = "user", key = "#user.username")
    public ResultVO cahce1(User user) {
        return ResultVOUtil.success("清除成功");
    }

    @GetMapping("/cache2")
    @LzcCache
    public ResultVO cahce2() {
        System.out.println("进来了");
        List<User> users = new ArrayList<>();
        User user1 = new User(1, "张三");
        User user2 = new User(2, "李四");
        User user3 = new User(3, "王麻子");
        users.add(user1);
        users.add(user2);
        users.add(user3);
        return ResultVOUtil.success(users);
    }

    @GetMapping("/cache3")
    @LzcCache(cacheName = "user", key = "#username")
    public ResultVO cahce3(@RequestParam Integer id, @RequestParam String username) {
        System.out.println("进来了");
        return ResultVOUtil.success(new User(id, username));
    }

    @GetMapping("/cache4")
    @LzcCache(cacheName = "user", key = "#id")
    public ResultVO cahce4(@RequestParam Integer id, @RequestParam String username) {
        System.out.println("进来了");
        return ResultVOUtil.success(new User(id, username));
    }

    @GetMapping("/cache5")
    @LzcCacheEvict(cacheName = "user", key = "#id")
    public ResultVO cahce5(@RequestParam Integer id, @RequestParam String username) {
        return ResultVOUtil.success();
    }
}
