package com.lzc.aop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class HelloAspect {
    @Around("execution(* com.lzc.aop.controller.*.*(..))") // 这里是指controller包下所有类的所有方法
    public Object helloAspect(ProceedingJoinPoint jp) {
        try {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            System.out.println("拦截地址：" + request.getRequestURI());
            Object result = jp.proceed(); // 获取请求返回的结果
            return result;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            System.out.println("拦截出现异常：");
        }
        return null;
    }
}
