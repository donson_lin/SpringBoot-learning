package com.lzc.cache.controller;

import com.lzc.cache.dataobject.User;
import com.lzc.cache.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cache")
public class CacheController {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/test1")
    public void user() {
        // stringRedisTemplate的基本用法：在测试类 CacheApplicationTests里面
    }
}
