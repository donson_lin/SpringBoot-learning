package com.lzc.cache.controller;

import com.lzc.cache.dataobject.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/cache")
    @Cacheable(cacheNames = "user")
    public User user(@RequestParam Integer id, @RequestParam String username) {
        System.out.println("进来了");
        return new User(id, username);
    }

    @GetMapping("/cache1")
    @Cacheable(cacheNames = "user", key = "#id")
    public User user1(@RequestParam Integer id) {
        System.out.println("进来了");
        return new User(id, "lizhencheng");
    }

    /**
     * 清除缓存
     *
     * @param id
     * @return
     */
    @GetMapping("/cache2")
    @CacheEvict(cacheNames = "user", key = "#id")
    public String clearUser(@RequestParam Integer id) {
        System.out.println("清除缓存");
        return "清除成功";
    }

    /**
     * 使用对象中的某个字段作为key
     *
     * @param user
     * @return
     */
    @GetMapping("/cache3")
    @Cacheable(cacheNames = "user", key = "#user.username")
    public User user3(User user) {
        System.out.println("进来了");
        return user;
    }

    /**
     * 当返回结果不为空时进行缓存
     *
     * @param id
     * @return
     */
    @GetMapping("/cache4")
    @Cacheable(cacheNames = "user", key = "#id", unless = "#result == null")
    public User user4(@RequestParam Integer id) {
        System.out.println("进来了");
        if (id == 1) {
            return null;
        }
        return new User(id, "lzihenchneg");
    }

}
