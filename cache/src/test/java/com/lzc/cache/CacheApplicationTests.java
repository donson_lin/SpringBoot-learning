package com.lzc.cache;

import com.lzc.cache.dataobject.User;
import com.lzc.cache.util.JsonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CacheApplicationTests {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void contextLoads() {

    }

    @Test
    public void test1() {
        User user = new User(1, "李振成");
        stringRedisTemplate.opsForValue().set("user_" + 1, JsonUtil.toJson(user));
    }

    @Test
    public void test2() {
        // 判断key是否存在
        if (stringRedisTemplate.hasKey("user_1")) {
            String result = stringRedisTemplate.opsForValue().get("user_1");
            User user = JsonUtil.toBean(User.class, result);
            System.out.println(user.toString());
        } else {
            System.out.println("不存在的key");
        }
    }

    @Test
    public void test3() {
        User user = new User(2, "张三");
        // 设置缓存为6分钟
        stringRedisTemplate.opsForValue().set("user_" + user.getId(), JsonUtil.toJson(user), 5, TimeUnit.MINUTES);
    }
}
