package com.lzc.cors.controller;

import com.lzc.cors.vo.ResultVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@CrossOrigin
@Controller
public class HelloController {


    @CrossOrigin(origins = {"*"})
    @PostMapping("/hello")
    @ResponseBody
    public ResultVO hello() {
        return new ResultVO(1, "成功");
    }

    @GetMapping("/index")
    public ModelAndView index() {
        return new ModelAndView("hello");
    }
}
