package com.lzc.cors.controller;

import com.lzc.cors.dataobject.User;
import com.lzc.cors.repository.UserRepository;
import com.lzc.cors.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/add")
    public ResultVO add(@Valid User user) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(1);
        resultVO.setMessage("成功");
        resultVO.setData(userRepository.save(user));
        return resultVO;
    }

    @GetMapping("/get")
    public ResultVO add() {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(1);
        resultVO.setMessage("成功");
        resultVO.setData(userRepository.findAll());
        return resultVO;
    }
}
