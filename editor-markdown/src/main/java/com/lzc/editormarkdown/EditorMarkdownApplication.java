package com.lzc.editormarkdown;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EditorMarkdownApplication {

	public static void main(String[] args) {
		SpringApplication.run(EditorMarkdownApplication.class, args);
	}
}
