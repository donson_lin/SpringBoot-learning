package com.lzc.editormarkdown.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;

/**
 * Create by lzc
 * Date 2018/8/20 09:45:03
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @GetMapping("/view")
    public ModelAndView view() {
        return new ModelAndView("view");
    }
}
