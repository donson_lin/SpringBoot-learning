package com.lzc.email.controller;

import com.lzc.email.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.validation.constraints.Email;
import java.util.UUID;

/**
 * Created by lzc
 * 2018/12/14 17:13
 */
@RestController
public class IndexController {

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private EmailService emailService;

    @GetMapping("/sendTemplate")
    public String sendTemplate() {
        //创建邮件正文
        Context context = new Context();
        context.setVariable("token", UUID.randomUUID().toString().replace("-",""));
        String emailContent = templateEngine.process("email/emailTemplate", context);
        emailService.sendHtmlEmail("923226145@qq.com","模板邮件",emailContent);
        return "发送成功";
    }
}
