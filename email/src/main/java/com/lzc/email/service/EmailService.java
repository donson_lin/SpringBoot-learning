package com.lzc.email.service;


import java.util.Set;

/** 发送邮件服务
 * Created by lzc
 * 2018/12/13 23:27
 */
public interface EmailService {
    /**
     * 发送简单邮件
     */
    public void sendSimpleEmail(String to, String subject, String content);

    /**
     * 发送html格式邮件
     */
    public void sendHtmlEmail(String to, String subject, String content);

    /**
     * 发送带附件的邮件
     */
    public void sendAttachmentsEmail(String to, String subject, String content, Set<String> filePaths);
}
