package com.lzc.email;

import com.lzc.email.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class EmailApplicationTests {
    @Autowired
    private EmailService emailService;

    @Autowired
    private TemplateEngine templateEngine;

    @Test
    public void contextLoads() {

    }

    /**
     * 发送简单邮件
     */
    @Test
    public void test1() {
        emailService.sendSimpleEmail("923226145@qq.com","简单邮件","简单邮件");
    }

    /**
     * 发送附件文件
     */
    @Test
    public void test2() {
        Set<String> files = new TreeSet<>();
        files.add("G:\\0019032438899352_b.jpg");
        files.add("G:\\10839717_103723525199_2.jpg");
        emailService.sendAttachmentsEmail("923226145@qq.com","发送附件","发送附件", files);
    }

    /**
     * 发送HTML邮件
     */
    @Test
    public void test3() {
        String content="<html>\n" +
                "<body>\n" +
                "    <h3>hello world ! 这是一封Html邮件!</h3>\n" +
                "</body>\n" +
                "</html>";
        emailService.sendHtmlEmail("923226145@qq.com","html邮件", content);
    }

    /**
     * 发送模板邮件
     */
    @Test
    public void test4() {
        //创建邮件正文
        Context context = new Context();
        context.setVariable("token", UUID.randomUUID().toString().replace("-",""));
        String emailContent = templateEngine.process("email/emailTemplate", context);
        emailService.sendHtmlEmail("923226145@qq.com","模板邮件",emailContent);
    }

}

