package com.lzc.fileserver.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultVo implements Serializable{

    private static final long serialVersionUID = 7849944011327109709L;
    /*错误码 0成功*/
    private Integer errorNo;
    /*提示信息*/
    private String errorInfo;
    /* 返回数据 */
    private Object results;
}
