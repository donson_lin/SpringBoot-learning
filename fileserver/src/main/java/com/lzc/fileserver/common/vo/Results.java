package com.lzc.fileserver.common.vo;

import lombok.Data;

@Data
public class Results {
    private Object data;
}
