package com.lzc.fileserver.file.repository;


import com.lzc.fileserver.file.dataobject.File;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FileRepository extends MongoRepository<File, String> {
}
