添加依赖

    <dependency>
    		    <groupId>org.springframework.boot</groupId>
    		    <artifactId>spring-boot-devtools</artifactId>
    		    <optional>true</optional>
    </dependency>
    
使用Intellij开发时，如果想要不手动重启服务，需要额外的配置，因为IDEA在非RUN或DEBUG情况下才会自动编译，具体操作如下：
（1）File-> Settings... -> Build,Execution,Deployment -> Compiler
找到 Make project automatically选项并勾
（2） Shift+Ctrl+Alt+/，选择Registry，进去之后找到
compiler.automake.allow.when.app.running并打勾
（3）重启项目即可