package com.lzc.hotdeploy;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create by lzc
 * Date 2018/8/28 10:07:49
 */
@RestController
public class HotDeployController {

    @GetMapping("/demo")
    public String demo() {
        return "hello springboot! i am lzc";
    }
}
