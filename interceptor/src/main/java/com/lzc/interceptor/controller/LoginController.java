package com.lzc.interceptor.controller;

import com.lzc.interceptor.dataobject.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class LoginController {

    @GetMapping("/login")
    public Object login(HttpServletRequest request, @RequestParam String username) {
        HttpSession httpSession = request.getSession();
        User user = new User(1, username);
        httpSession.setAttribute("LOGIN_USER", user);
        return user;
    }

    @GetMapping(value = "/user")
    public User user(HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        User user = (User) httpSession.getAttribute("LOGIN_USER");
        return user;
    }

    @GetMapping("/user/index")
    public User index(HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        User user = (User) httpSession.getAttribute("LOGIN_USER");
        return user;
    }
}
