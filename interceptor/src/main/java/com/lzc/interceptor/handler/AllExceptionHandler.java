package com.lzc.interceptor.handler;

import com.lzc.interceptor.exception.LoginExcewption;
import com.lzc.interceptor.utils.ResultVOUtil;
import com.lzc.interceptor.vo.ResultVO;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AllExceptionHandler {

    @ExceptionHandler(value = LoginExcewption.class)
    public ResultVO loginExcetion(Exception e) {
        System.out.println("【ExceptionHandler】" + e.getMessage());
        return ResultVOUtil.success(e.getMessage());
    }

}
