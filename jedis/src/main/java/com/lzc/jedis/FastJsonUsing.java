package com.lzc.jedis;

import com.alibaba.fastjson.JSON;
import com.lzc.jedis.dataobject.Product;

import java.math.BigDecimal;

public class FastJsonUsing {
    public static void main(String[] args) {
        Product product = new Product(1,"桂林米粉",new BigDecimal(20.5));
        String str = JSON.toJSONString(product); // 将对象转换成json字符串
        System.out.println(str);

        System.out.println("##################");

        Product product1 = JSON.parseObject(str, Product.class); // 将json字符串转换成对象
        System.out.println(product1.toString());
    }
}
