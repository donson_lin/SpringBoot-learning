package com.lzc.jedis.util;

import com.lzc.jedis.common.RedisPool;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

@Slf4j
public class RedisPoolUtil {

    /**
     * 设置key的有效期，单位是秒
     * @param key
     * @param exTime
     * @return
     */
    public static Long expire(String key,int exTime){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.expire(key,exTime);
        } catch (Exception e) {
            log.error("expire key:{} error",key,e);
            jedis.close();
            return result;
        }
        jedis.close();
        return result;
    }

    // exTime的单位是秒
    public static String setEx(String key,String value,int exTime){
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.setex(key,exTime,value);
        } catch (Exception e) {
            log.error("setex key:{} value:{} error",key,value,e);
            jedis.close();
            return result;
        }
        jedis.close();
        return result;
    }

    public static String set(String key,String value){
        Jedis jedis = null;
        String result = null;

        try {
            jedis = RedisPool.getJedis();
            result = jedis.set(key,value);
        } catch (Exception e) {
            log.error("set key:{} value:{} error",key,value,e);
            jedis.close();
            return result;
        }
        jedis.close();
        return result;
    }

    public static boolean exists(String key){
        Jedis jedis = null;
        boolean result = false;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.exists(key);
        } catch (Exception e) {
            log.error("exists key:{}, error",key,e);
            jedis.close();
            return result;
        }
        jedis.close();
        return result;
    }

    /**
     *
     * @param key
     * @param value
     * @return 1 设置成功， 0 或者 null 失败
     */
    public static Long setnx(String key,String value){
        Jedis jedis = null;
        Long result = null;

        try {
            jedis = RedisPool.getJedis();
            result = jedis.setnx(key,value);
        } catch (Exception e) {
            log.error("setnx key:{} value:{} error",key,value,e);
            jedis.close();
            return result;
        }

        jedis.close();
        return result;
    }

    public static String getSet(String key,String value){
        Jedis jedis = null;
        String result = null;

        try {
            jedis = RedisPool.getJedis();
            result = jedis.getSet(key,value);
        } catch (Exception e) {
            log.error("setnx key:{} value:{} error",key,value,e);
            jedis.close();
            return result;
        }

        jedis.close();
        return result;
    }

    public static String get(String key){
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.get(key);
        } catch (Exception e) {
            log.error("get key:{} error",key,e);
            jedis.close();
            return result;
        }
        jedis.close();
        return result;
    }

    public static Long del(String key){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.del(key);
        } catch (Exception e) {
            log.error("del key:{} error",key,e);
            jedis.close();
            return result;
        }
        jedis.close();
        return result;
    }

    public static void main(String[] args) {

    }
}
