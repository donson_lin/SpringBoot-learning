package com.lzc.loginauth.annotation;

import com.lzc.loginauth.enums.AuthReturnType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginAuth {
    AuthReturnType authReturnType() default AuthReturnType.JSON;
}
