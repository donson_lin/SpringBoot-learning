package com.lzc.loginauth.controller;

import com.lzc.loginauth.annotation.LoginAuth;
import com.lzc.loginauth.constant.LoginConstant;
import com.lzc.loginauth.dataobject.AuthUser;
import com.lzc.loginauth.utils.ResultVOUtil;
import com.lzc.loginauth.vo.ResultVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

@RestController
public class HelloController {
    @GetMapping("/login")
    public ResultVO login(HttpServletRequest request, @RequestParam Integer id, @RequestParam String username) {
        HttpSession httpSession = request.getSession();
        AuthUser authUser = new AuthUser();
        authUser.setId(id);
        authUser.setUsername(username);
        authUser.setCertification(true);
        authUser.setLoginTime(new Date());
        httpSession.setAttribute(LoginConstant.AUTH_USER, authUser);
        return ResultVOUtil.success(authUser);
    }
}
