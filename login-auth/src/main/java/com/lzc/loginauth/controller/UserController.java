package com.lzc.loginauth.controller;

import com.lzc.loginauth.annotation.LoginAuth;
import com.lzc.loginauth.constant.LoginConstant;
import com.lzc.loginauth.dataobject.AuthUser;
import com.lzc.loginauth.enums.AuthReturnType;
import com.lzc.loginauth.utils.ResultVOUtil;
import com.lzc.loginauth.vo.ResultVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/index1")
    @LoginAuth(authReturnType = AuthReturnType.HTML)
    public ResultVO index1(AuthUser authUser) {
        return ResultVOUtil.success(authUser);
    }

    @GetMapping("/index2")
    @LoginAuth
    public ResultVO index2(AuthUser authUser) {
        return ResultVOUtil.success(authUser);
    }
}
