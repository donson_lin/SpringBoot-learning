package com.lzc.loginauth.dataobject;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class AuthUser {

    private Integer id;

    private String username;

    private boolean isCertification;

    @JsonFormat(pattern = "yyyy-MM-dd HH:ss:mm")
    private Date loginTime;

    public AuthUser() {
    }

    public AuthUser(Integer id, String username, boolean isCertification, Date loginTime) {
        this.id = id;
        this.username = username;
        this.isCertification = isCertification;
        this.loginTime = loginTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isCertification() {
        return isCertification;
    }

    public void setCertification(boolean certification) {
        isCertification = certification;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }
}
