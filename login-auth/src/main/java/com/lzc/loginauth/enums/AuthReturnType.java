package com.lzc.loginauth.enums;

/**
 * 当验证失败时返回html页面或者Json数据
 */
public enum AuthReturnType {
    JSON("json"),
    HTML("html");
    private String type;

    AuthReturnType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
