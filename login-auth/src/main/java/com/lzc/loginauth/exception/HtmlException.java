package com.lzc.loginauth.exception;

public class HtmlException extends RuntimeException {
    public HtmlException(String message) {
        super(message);
    }
}
