package com.lzc.loginauth.handler;

import com.lzc.loginauth.exception.HtmlException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class HtmlExceptionHandler {

    @ExceptionHandler(value = HtmlException.class)
    public ModelAndView htmlException(HtmlException e, Model model) {
        model.addAttribute("errorMessage", e.getMessage());
        return new ModelAndView("errorMessage");
    }
}
