package com.hfp.mall.common.handler;

import com.hfp.mall.common.util.ResultVOUtil;
import com.hfp.mall.common.vo.ResultVo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class MallExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultVo handlerSellException(Exception e) {
        return ResultVOUtil.error(-1,e.getMessage());
    }
}
