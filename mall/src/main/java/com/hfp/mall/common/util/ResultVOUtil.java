package com.hfp.mall.common.util;

import com.hfp.mall.common.vo.ResultVo;
import com.hfp.mall.common.vo.Results;
import com.hfp.mall.common.vo.ReturnPageData;

public class ResultVOUtil {

    public static ResultVo success() {
        ResultVo resultVo = new ResultVo();
        resultVo.setErrorNo(0);
        return resultVo;
    }


    public static ResultVo success(Object data, Integer type) {
        ResultVo resultVo = new ResultVo();
        resultVo.setErrorNo(0);
        resultVo.setResults(data);
        return resultVo;
    }

    public static ResultVo success(Object data) {
        ResultVo resultVo = new ResultVo();
        resultVo.setErrorNo(0);

        Results results = new Results();
        results.setData(data);

        resultVo.setResults(results);

        return resultVo;
    }

    public static ResultVo error(Integer errorNo,String errorInfo) {
        ResultVo resultVo = new ResultVo();
        resultVo.setErrorNo(errorNo);
        resultVo.setErrorInfo(errorInfo);
        return resultVo;
    }
}
