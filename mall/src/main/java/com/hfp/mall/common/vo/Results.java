package com.hfp.mall.common.vo;

import lombok.Data;

@Data
public class Results {
    private Object data;
}
