package com.hfp.mall.common.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;


public class ReturnPageData {
    /*具体内容*/
    private Object list;
    /*页码*/
    private Integer pageNum;
    /*每页数量*/
    private Integer pageSize;
    /*总数量*/
    private Long total;
    /*当前页*/
    private Long pages;

    public ReturnPageData() {
    }

    public ReturnPageData(IPage iPage, Integer pageNum, Integer pageSize) {
        this.list = iPage.getRecords();
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = iPage.getTotal();
        this.pages = iPage.getPages();
    }


    public ReturnPageData(Object list, Integer pageNum, Integer pageSize, Long total, Long pages) {
        this.list = list;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = total;
        this.pages = pages;
    }

    public Object getList() {
        return list;
    }

    public void setList(Object list) {
        this.list = list;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getPages() {
        return pages;
    }

    public void setPages(Long pages) {
        this.pages = pages;
    }
}
