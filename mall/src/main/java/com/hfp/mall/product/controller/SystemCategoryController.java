package com.hfp.mall.product.controller;


import com.hfp.mall.common.util.ResultVOUtil;
import com.hfp.mall.common.vo.ResultVo;
import com.hfp.mall.product.service.ISystemCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统分类表 前端控制器
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
@RestController
@RequestMapping("/product")
public class SystemCategoryController {

    @Autowired
    private ISystemCategoryService systemCategoryService;

    @GetMapping("/systemCategorys")
    public ResultVo systemCategoryList() {
        return ResultVOUtil.success(systemCategoryService.list());
    }
}
