package com.hfp.mall.product.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品信息表
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Product implements Serializable {


    private static final long serialVersionUID = -3238752454164496825L;

    /**
     * 商品id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 系统分类id
     */
    private Integer systemCategoryId;

    /**
     * 商家id
     */
    private Integer merchantId;

    /**
     * 商品标题
     */
    private String name;

    /**
     * 商品描述
     */
    private String intro;

    /**
     * 状态 -1=>下架,1=>上架,2=>预售,0=>未上架
     */
    private Integer status;

    /**
     * 审核状态 -1 审核失败 0 未审核 1 审核成功
     */
    private Integer state;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 封面图
     */
    private String pictureUrl;

    /**
     * 展示图片地址，每个地址逗号分隔
     */
    private String pictureList;

    /**
     * 商品所在店名
     */
    private String merchantName;

    /**
     * 销售量
     */
    private Integer salesVolume;

    /**
     * 评论数量
     */
    private Integer comment;

    /**
     * 0国产，1进口
     */
    private Integer countryType;

    /**
     * 运费金额
     */
    private BigDecimal logisticsFee;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;


}
