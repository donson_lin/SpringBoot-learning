package com.hfp.mall.product.form;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ProductForm {

    private Integer id;

    /**
     * 系统分类id
     */
    private Integer systemCategoryId;

    /**
     * 商品标题
     */
    private String name;

    /**
     * 商品描述
     */
    private String intro;

    /**
     * 状态 -1=>下架,1=>上架,2=>预售,0=>未上架
     */
    private Integer status;


    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 封面图
     */
    private String pictureUrl;

    /**
     * 展示图片地址，每个地址逗号分隔
     */
    private String pictureList;

    /**
     * 0国产，1进口
     */
    private Integer countryType;

    /**
     * 运费金额
     */
    private BigDecimal logisticsFee;

}
