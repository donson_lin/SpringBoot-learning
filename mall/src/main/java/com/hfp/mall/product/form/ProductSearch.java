package com.hfp.mall.product.form;

import lombok.Data;

@Data
public class ProductSearch {

    /**
     * 系统分类id
     */
    private Integer systemCategoryId;

    /**
     * 商品标题
     */
    private String name;

    /**
     * 状态 -1=>下架,1=>上架,2=>预售,0=>未上架
     */
    private Integer status;

    /**
     * 0国产，1进口
     */
    private Integer countryType;
}
