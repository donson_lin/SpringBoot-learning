package com.hfp.mall.product.mapper;

import com.hfp.mall.product.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品信息表 Mapper 接口
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
public interface ProductMapper extends BaseMapper<Product> {

}
