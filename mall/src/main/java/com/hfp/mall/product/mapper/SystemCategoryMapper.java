package com.hfp.mall.product.mapper;

import com.hfp.mall.product.entity.SystemCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统分类表 Mapper 接口
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
public interface SystemCategoryMapper extends BaseMapper<SystemCategory> {

}
