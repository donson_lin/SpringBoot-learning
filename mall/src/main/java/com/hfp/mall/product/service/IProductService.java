package com.hfp.mall.product.service;

import com.hfp.mall.product.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品信息表 服务类
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
public interface IProductService extends IService<Product> {

}
