package com.hfp.mall.product.service;

import com.hfp.mall.product.entity.SystemCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统分类表 服务类
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
public interface ISystemCategoryService extends IService<SystemCategory> {

}
