package com.hfp.mall.product.service.impl;

import com.hfp.mall.product.entity.Product;
import com.hfp.mall.product.mapper.ProductMapper;
import com.hfp.mall.product.service.IProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品信息表 服务实现类
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

}
