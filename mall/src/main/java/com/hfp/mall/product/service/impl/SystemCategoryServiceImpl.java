package com.hfp.mall.product.service.impl;

import com.hfp.mall.product.entity.SystemCategory;
import com.hfp.mall.product.mapper.SystemCategoryMapper;
import com.hfp.mall.product.service.ISystemCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统分类表 服务实现类
 * </p>
 *
 * @author lzc
 * @since 2019-01-22
 */
@Service
public class SystemCategoryServiceImpl extends ServiceImpl<SystemCategoryMapper, SystemCategory> implements ISystemCategoryService {

}
