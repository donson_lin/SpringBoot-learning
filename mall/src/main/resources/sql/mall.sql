/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : mall

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 24/01/2019 15:04:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '城市',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `region` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地区',
  `street` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '街道',
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详细地址',
  `default_address` tinyint(4) NOT NULL DEFAULT 0 COMMENT '默认地址  1是 2否',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户收货地址' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for merchant
-- ----------------------------
DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商家名字',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `intro` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '店家简介',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '店铺头像',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商家信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `merchant_id` int(11) NOT NULL COMMENT '店铺id',
  `order_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单编号',
  `buyer_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '买家姓名',
  `buyer_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '买家电话',
  `buyer_address` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '买家地址',
  `product_count` int(11) NOT NULL COMMENT '商品数量',
  `order_amount` decimal(8, 2) NOT NULL COMMENT '订单总金额',
  `order_status` tinyint(4) NULL DEFAULT 0 COMMENT '订单状态 0未付款,1已付款,2已发货,3已签收,-1退货申请,-2退货中,-3已退货,-4取消交易 -5撤销申请',
  `after_status` tinyint(4) NULL DEFAULT 0 COMMENT '用户售后状态  0 未发起售后  1 申请售后  -1 售后已取消  2 处理中  200 处理完毕',
  `logistics_fee` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '运费金额',
  `pay_channel` tinyint(4) NOT NULL COMMENT '支付渠道 0余额 1微信 2支付宝',
  `out_trade_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `escrow_trade_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方支付流水号',
  `pay_time` timestamp(0) NULL DEFAULT NULL COMMENT '支付时间',
  `delivery_time` timestamp(0) NULL DEFAULT NULL COMMENT '发货时间',
  `delete_time` timestamp(0) NULL DEFAULT NULL COMMENT '删除时间',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_merchant_id`(`merchant_id`) USING BTREE,
  INDEX `idx_user_id_merchant_id_order_no_order_status`(`user_id`, `merchant_id`, `order_no`, `order_status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL COMMENT '订单编号',
  `product_id` int(11) NOT NULL COMMENT '商品id',
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名',
  `product_price` decimal(8, 2) NOT NULL COMMENT '商品价格',
  `product_picture_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品图片',
  `product_quantity` int(11) NOT NULL COMMENT '商品数量',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE,
  INDEX `idx_product_id`(`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `merchant_category_id` int(11) NULL DEFAULT NULL COMMENT '商家分类id',
  `system_category_id` int(11) NOT NULL COMMENT '系统分类id',
  `merchant_id` int(11) NOT NULL COMMENT '商家id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品标题',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品描述',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '状态 -1=>下架,1=>上架,2=>预售,0=>未上架',
  `state` tinyint(4) NULL DEFAULT 0 COMMENT '审核状态 -1 审核失败 0 未审核 1 审核成功',
  `price` decimal(8, 2) NOT NULL COMMENT '价格',
  `stock` int(11) NOT NULL DEFAULT 0 COMMENT '库存',
  `picture_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `picture_list` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '展示图片地址，每个地址逗号分隔',
  `merchant_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品所在店名',
  `sales_volume` int(11) NULL DEFAULT 0 COMMENT '销售量',
  `comment` int(11) NULL DEFAULT 0 COMMENT '评论数量',
  `country_type` tinyint(4) NULL DEFAULT 0 COMMENT '0国产，1进口',
  `logistics_fee` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '运费金额',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_product_merchant_category_id`(`merchant_category_id`) USING BTREE,
  INDEX `idx_product_system_category_id`(`system_category_id`) USING BTREE,
  INDEX `idx_merchant_id`(`merchant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (14, NULL, 1, 1, '苹果', '<p>苹果好吃又不贵</p><p><img src=\"http://localhost:8081/file/files/view/5c4943e7fc28f4476411e315\" width=\"330\" height=\"330\" alt=\"\"><br data-mce-bogus=\"1\"></p>', 1, 0, 20.00, 100, 'http://localhost:8081/file/files/view/5c4943c4fc28f4476411e312', 'http://localhost:8081/file/files/view/5c4943c7fc28f4476411e313', '桂林米粉店', 0, 0, 0, 0.00, '2019-01-24 12:49:47', '2019-01-24 12:49:47');
INSERT INTO `product` VALUES (15, NULL, 2, 1, '猕猴桃123', '<p>123123</p><p><img src=\"http://localhost:8081/file/files/view/5c495513fc28f4476411e326\" width=\"330\" height=\"330\" alt=\"\" data-mce-src=\"http://localhost:8081/file/files/view/5c495513fc28f4476411e326\"></p>', -1, 0, 20.00, 100, 'http://localhost:8081/file/files/view/5c495288fc28f4476411e31e', 'http://localhost:8081/file/files/view/5c49528bfc28f4476411e31f,http://localhost:8081/file/files/view/5c49550bfc28f4476411e325', '桂林米粉店', 0, 0, 0, 0.00, '2019-01-24 13:55:19', '2019-01-24 14:16:45');
INSERT INTO `product` VALUES (17, NULL, 2, 1, '樱桃', '<p>78945222222222222222222222222222111111111111111111111111111111111111111</p><p>123456</p><p><img src=\"http://localhost:8081/file/files/view/5c495878fc28f4476411e32b\" width=\"330\" height=\"330\" alt=\"\" data-mce-src=\"http://localhost:8081/file/files/view/5c495878fc28f4476411e32b\"></p>', -1, 0, 50.00, 100, 'http://localhost:8081/file/files/view/5c49586dfc28f4476411e329', 'http://localhost:8081/file/files/view/5c495871fc28f4476411e32a', '桂林米粉店', 0, 0, 1, 0.00, '2019-01-24 14:17:31', '2019-01-24 14:18:12');
INSERT INTO `product` VALUES (18, NULL, 1, 1, '1', '<p>111111111</p>', -1, 0, 1.00, 10, 'http://localhost:8081/file/files/view/5c495ef2fc28f4476411e32c', 'http://localhost:8081/file/files/view/5c495ef5fc28f4476411e32d', '桂林米粉店', 0, 0, 0, 0.00, '2019-01-24 14:45:16', '2019-01-24 14:45:16');
INSERT INTO `product` VALUES (19, NULL, 1, 1, '1', '<p>1111111111111111111</p>', 1, 0, 1.00, 1, 'http://localhost:8081/file/files/view/5c495f08fc28f4476411e32e', 'http://localhost:8081/file/files/view/5c495f0cfc28f4476411e32f', '桂林米粉店', 0, 0, 1, 0.00, '2019-01-24 14:45:35', '2019-01-24 14:45:35');
INSERT INTO `product` VALUES (20, NULL, 1, 1, '10', '<p>11111111111111</p>', 1, 0, 10.00, 1, 'http://localhost:8081/file/files/view/5c495f79fc28f4476411e334', 'http://localhost:8081/file/files/view/5c495f7cfc28f4476411e335', '桂林米粉店', 0, 0, 0, 0.00, '2019-01-24 14:47:27', '2019-01-24 14:47:27');
INSERT INTO `product` VALUES (21, NULL, 1, 1, '1', '<p>111111111111111111</p>', -1, 0, 1.00, 1, 'http://localhost:8081/file/files/view/5c495f8cfc28f4476411e336', 'http://localhost:8081/file/files/view/5c495f8ffc28f4476411e337', '桂林米粉店', 0, 0, 0, 0.00, '2019-01-24 14:47:46', '2019-01-24 14:47:46');
INSERT INTO `product` VALUES (22, NULL, 1, 1, '1', '<p>11111111111111111111111111</p>', 0, 0, 1.00, 1, 'http://localhost:8081/file/files/view/5c495f9efc28f4476411e338', 'http://localhost:8081/file/files/view/5c495fa1fc28f4476411e339', '桂林米粉店', 0, 0, 1, 0.00, '2019-01-24 14:48:10', '2019-01-24 14:48:10');
INSERT INTO `product` VALUES (23, NULL, 1, 1, '1', '<p>11111111111111111111111</p>', -1, 0, 1.00, 1, 'http://localhost:8081/file/files/view/5c495fb4fc28f4476411e33a', 'http://localhost:8081/file/files/view/5c495fb7fc28f4476411e33b', '桂林米粉店', 0, 0, 0, 0.00, '2019-01-24 14:48:26', '2019-01-24 14:48:26');
INSERT INTO `product` VALUES (24, NULL, 1, 1, '11', '<p>1111111111111</p>', -1, 0, 11.00, 11, 'http://localhost:8081/file/files/view/5c495fc7fc28f4476411e33c', 'http://localhost:8081/file/files/view/5c495fcafc28f4476411e33d', '桂林米粉店', 0, 0, 1, 0.00, '2019-01-24 14:48:45', '2019-01-24 14:48:45');
INSERT INTO `product` VALUES (25, NULL, 1, 1, '1', '<p>1111111111</p>', -1, 0, 111111.00, 111, 'http://localhost:8081/file/files/view/5c495fe2fc28f4476411e33e', 'http://localhost:8081/file/files/view/5c495fe5fc28f4476411e33f', '桂林米粉店', 0, 0, 0, 0.00, '2019-01-24 14:49:12', '2019-01-24 14:49:12');

-- ----------------------------
-- Table structure for product_attribute_name
-- ----------------------------
DROP TABLE IF EXISTS `product_attribute_name`;
CREATE TABLE `product_attribute_name`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `product_id` int(11) NOT NULL COMMENT '商品id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '属性名',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_product_id`(`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品属性名' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_attribute_value
-- ----------------------------
DROP TABLE IF EXISTS `product_attribute_value`;
CREATE TABLE `product_attribute_value`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_attribute_name_id` int(11) NOT NULL COMMENT '商品属性名id',
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品属性值',
  `sort` int(11) NULL DEFAULT 0,
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_product_attribute_name_id`(`product_attribute_name_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品属性值' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `product_id` int(11) NOT NULL COMMENT '商品id',
  `merchant_id` int(11) NOT NULL COMMENT '商家id',
  `merchant_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商家民称',
  `product_quantity` int(11) NOT NULL COMMENT '商品数量',
  `checked` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否选择,1=已勾选,0=未勾选',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_product_id`(`product_id`) USING BTREE,
  INDEX `idx_merchant_id`(`merchant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_category
-- ----------------------------
DROP TABLE IF EXISTS `system_category`;
CREATE TABLE `system_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统分类id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统分类名',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_category
-- ----------------------------
INSERT INTO `system_category` VALUES (1, '苹果', 0, '2019-01-22 17:49:27', '2019-01-22 17:49:27');
INSERT INTO `system_category` VALUES (2, '桃子', 0, '2019-01-22 17:49:48', '2019-01-22 17:49:48');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
