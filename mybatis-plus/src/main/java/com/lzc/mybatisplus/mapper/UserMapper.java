package com.lzc.mybatisplus.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lzc.mybatisplus.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Create by lzc
 * Date 2018/8/31 11:34:35
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select("select id, username from user")
    List<User> getUserList(Page page);

    User getUserById(Integer id);

}
