package com.lzc.mybatisplus.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.apache.ibatis.annotations.SelectKey;

/**
 * Create by lzc
 * Date 2018/8/31 11:35:06
 */
@TableName("user")
public class User {
    // 如果是自增id，需要使用这个注解
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String username;

    private String deptId;

    public User() {
    }

    public User(Integer id, String username, String deptId) {
        this.id = id;
        this.username = username;
        this.deptId = deptId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", deptId='" + deptId + '\'' +
                '}';
    }
}
