package com.lzc.mybatisplus;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lzc.mybatisplus.mapper.UserMapper;
import com.lzc.mybatisplus.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MybatisPlusApplicationTests {

	@Autowired
	UserMapper userMapper;

	@Test
	public void contextLoads() {

		User user = new User();
		user.setUsername("lzc");
		user.setDeptId("123456789");
		userMapper.insert(user);
		System.out.println(user.toString());
	}

	@Test
	public void test1() {
		// Page<User> page = new Page<>(1,10,"id", false);
		Page<User> page = new Page<>();
		for (User u : userMapper.getUserList(page)) {
			System.out.println(u.toString());
		}
		System.out.println(page.getTotal());
	}

	@Test
	public void test2() {
		System.out.println(userMapper.getUserById(1).toString());
	}

	@Test
	public void page1() {
		List<User> userList = userMapper.selectList(
				new EntityWrapper<User>().eq("username", "张三")
		);
		for (User u : userList) {
			System.out.println(u.toString());
		}
	}

	@Test
	public void page2() {
		List<String> list = new ArrayList<>();
		list.add("id");

		Page<User> page = new Page<>(4,10);
		page.setDescs(list);

		page.setRecords(userMapper.selectPage(page,new EntityWrapper<User>()));
		System.out.println("getCurrent:"+page.getCurrent());
		System.out.println("getPages:"+page.getPages());
		System.out.println("getSize:"+page.getSize());
		System.out.println("getSize:"+page.getTotal());
		for (User u : page.getRecords()) {
			System.out.println(u.toString());
		}
		System.out.println(page.getRecords().toString());
	}

	@Test
	public void page3() {
		Map<String, Object> map = new HashMap<>();
		map.put("username","张三");
		map.put("dept_id","2");
		List<User> userList = userMapper.selectByMap(map);
		for (User u : userList) {
			System.out.println(u.toString());
		}
	}

	// 查询 + 分页
	@Test
	public void page4() {
		Map<String, Object> map = new HashMap<>();
		map.put("username","张三");
		map.put("dept_id","2");

		Page<User> page = new Page<>(1,10);

		List<User> userList = userMapper.selectPage(page, new EntityWrapper<User>().allEq(map));

		for (User u : userList) {
			System.out.println(u.toString());
		}
	}

}
