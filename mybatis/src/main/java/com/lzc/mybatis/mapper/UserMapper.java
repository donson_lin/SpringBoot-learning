package com.lzc.mybatis.mapper;

import com.lzc.mybatis.dataobject.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;


@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Insert("insert into user(name, password) values(#{name}, #{password})")
    @Options(useGeneratedKeys = true, keyProperty = "userId")
    int insert(User user);

    @Select("select * from user where user_id = #{id}")
    User getById(Integer id);

    @Select("select * from user")
    List<User> getAll();

    int insertAll(List<User> users);

    int deleteAll(List<Integer> ids);
}
