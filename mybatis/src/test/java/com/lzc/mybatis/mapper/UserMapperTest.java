package com.lzc.mybatis.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzc.mybatis.dataobject.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void insert() {
        User user = new User("lzc", "123456");
        userMapper.insert(user);
        System.out.println(user.toString());
    }

    @Test
    public void getById() {
        User user = userMapper.getById(6);
        System.out.println(user);
    }

    @Test
    public void getAll() {
        List<User> users = new ArrayList<>();
        PageHelper.startPage(2, 4).setOrderBy("user_id desc");
        PageInfo<User> userPageInfo = new PageInfo<>(userMapper.getAll());
        System.out.println(userPageInfo);
    }

    /**
     * 通用mapper插件可以实现简单的增删改查
     */
    @Test
    public void insertSelective() {
        User user = new User("lzclzcl", "123456");
        userMapper.insertSelective(user);  // 插入
        userMapper.deleteByPrimaryKey(13); // 通过主键删除
        userMapper.selectAll(); // 获取所有
        userMapper.updateByPrimaryKey(user); // 修改
    }
}