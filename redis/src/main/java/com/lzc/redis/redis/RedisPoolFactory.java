package com.lzc.redis.redis;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisPoolFactory {
    @Bean
    public JedisPool JedisPoolFactory() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxIdle(500); //最小空闲连接数, 默认0
        poolConfig.setMaxTotal(1000); //最大连接数, 默认8个
        // 连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
        poolConfig.setMaxWaitMillis(500 * 1000);
        JedisPool jp = new JedisPool(poolConfig, "127.0.0.1", 6379, 3 * 1000);
        // JedisPool jp = new JedisPool(poolConfig, "127.0.0.1", 6379, 3*1000,"llmllm",0); // 后面两个参数分别为密码、数据库
        return jp;
    }
}
