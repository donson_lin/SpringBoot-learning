package com.lzc.redis;

import com.lzc.redis.dataobject.User;
import com.lzc.redis.redis.RedisService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisApplicationTests {

    @Autowired
    private RedisService redisService;

    @Test
    public void contextLoads() {
        User user = new User(123, "Jedis");
        redisService.set("user_" + user.getId(), user, 100);
    }

    @Test
    public void test1() {
        User user = redisService.get("user_1", User.class);
        System.out.println(user);
    }

}
