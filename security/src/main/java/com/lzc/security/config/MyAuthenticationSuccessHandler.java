package com.lzc.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lzc.security.enums.LoginType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class MyAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        System.out.println("登录成功");
        if (LoginType.JSON.equals(securityProperties.getLoginType())) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", "0");
            map.put("msg", "成功");
            map.put("data", authentication);

            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(authentication));
        } else {
            super.setDefaultTargetUrl(securityProperties.getDefaultSuccessUrl());
            super.onAuthenticationSuccess(request, response, authentication);
        }
    }
}
