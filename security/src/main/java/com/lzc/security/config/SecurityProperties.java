package com.lzc.security.config;

import com.lzc.security.enums.LoginType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "lzc.security")
public class SecurityProperties {

    private LoginType loginType;

    private String defaultSuccessUrl;

    private String setDefaultFailureUrl;

    public SecurityProperties() {
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public String getDefaultSuccessUrl() {
        return defaultSuccessUrl;
    }

    public void setDefaultSuccessUrl(String defaultSuccessUrl) {
        this.defaultSuccessUrl = defaultSuccessUrl;
    }

    public String getSetDefaultFailureUrl() {
        return setDefaultFailureUrl;
    }

    public void setSetDefaultFailureUrl(String setDefaultFailureUrl) {
        this.setDefaultFailureUrl = setDefaultFailureUrl;
    }
}
