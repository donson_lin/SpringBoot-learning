package com.lzc.shell.controller;

import com.lzc.shell.dto.OrderDTO;
import com.lzc.shell.enums.ResultEnum;
import com.lzc.shell.exception.SellException;
import com.lzc.shell.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/pay")
public class PayController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/create")
    public void create(@RequestParam("orderId")String orderId,@RequestParam("returnUrl")String returnUrl){

        //1.查询订单
        OrderDTO orderDTO = orderService.findOne(orderId);
        if(orderDTO==null){
            throw new SellException(ResultEnum.ORDER_STOCK_ERROR);
        }
        //发起支付
    }
}
