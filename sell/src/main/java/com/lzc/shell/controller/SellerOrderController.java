package com.lzc.shell.controller;

import com.lzc.shell.dto.OrderDTO;
import com.lzc.shell.enums.ResultEnum;
import com.lzc.shell.exception.SellException;
import com.lzc.shell.service.OrderService;
import com.lzc.shell.service.impl.OrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/seller/order")
@Slf4j
public class SellerOrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/list")
    public ModelAndView list(@RequestParam(value = "page",defaultValue = "1") Integer page,
                              @RequestParam(value = "size" , defaultValue = "3") Integer size,
                             Map<String,Object>map){

        List<Sort.Order> orders = new ArrayList<>();
        orders.add(new Sort.Order(Sort.Direction.DESC,"createTime"));
        orders.add(new Sort.Order(Sort.Direction.ASC,"orderStatus"));

        PageRequest request = new PageRequest(page-1,size,new Sort(orders));
        Page<OrderDTO> orderDTOPage = orderService.findList(request);
        map.put("orderDTOPage",orderDTOPage);
        map.put("currentPage",page);
        map.put("size",size);
        return new ModelAndView("order/list",map);
    }

    @GetMapping("/cancel")
    public ModelAndView cancel(@RequestParam("orderId") String orderId,
                                @RequestParam("page") String page,
                                @RequestParam("size") String size,
                                Map<String,Object>map)
    {
        try {
            OrderDTO orderDTO = orderService.findOne(orderId);
            orderService.cancel(orderDTO);
        }catch (SellException e){
            log.error("【买家端取消订单】 发生异常{ }", e);
            map.put("msg", e.getMessage());
            map.put("url","/sell/seller/order/list?page="+page+"&size="+size);
            return new ModelAndView("/common/error",map);
        }
        map.put("msg", ResultEnum.ORDER_CANCEL_SUCCESS.getMessage());
        map.put("url","/sell/seller/order/list?page="+page+"&size="+size);
        return new ModelAndView("/common/success",map);
    }

    /*
        订单详情
     */
    @GetMapping("/detail")
    public ModelAndView detail(@RequestParam("orderId") String orderId,
                                @RequestParam("page") String page,
                                @RequestParam("size") String size,
                                Map<String, Object> map) {
        OrderDTO orderDTO = new OrderDTO();
        try {
            orderDTO = orderService.findOne(orderId);
        }catch (SellException e) {
            log.error("【卖家端查询订单详情】发生异常{}", e);
            map.put("msg", e.getMessage());
            map.put("url","/sell/seller/order/list?page="+page+"&size="+size);
            return new ModelAndView("common/error", map);
        }

        map.put("orderDTO", orderDTO);
        map.put("page",page);
        map.put("size",size);
        return new ModelAndView("order/details", map);
    }

    @GetMapping("/finish")
    public ModelAndView finished(@RequestParam("orderId") String orderId,
                                 @RequestParam("page") String page,
                                 @RequestParam("size") String size,
                                 Map<String, Object> map) {
        try {
            OrderDTO orderDTO = orderService.findOne(orderId);
            orderService.finish(orderDTO);
        } catch (SellException e) {
            log.error("【卖家端完结订单】发生异常{}", e);
            map.put("msg", e.getMessage());
            map.put("url","/sell/seller/order/list?page="+page+"&size="+size);
            return new ModelAndView("common/error", map);
        }

        map.put("msg", ResultEnum.ORDER_FINISH_SUCCESS.getMessage());
        map.put("url","/sell/seller/order/list?page="+page+"&size="+size);
        return new ModelAndView("common/success");
    }

}
