package com.lzc.shell.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/weixin")
@Slf4j
public class WeiXinController {

    @GetMapping("/auth")
    public void auth(@RequestParam("code")String code)
    {
        String href = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx7026a4548c27b218&secret=8068d3c6fd0ca8786d7546db6b297332&code="+code+"&grant_type=authorization_code";
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(href,String.class);
        log.info(response);
    }
}
