package com.lzc.shell.converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lzc.shell.dataobject.OrderDetail;
import com.lzc.shell.dto.OrderDTO;
import com.lzc.shell.enums.ResultEnum;
import com.lzc.shell.exception.SellException;
import com.lzc.shell.form.OrderForm;

import java.util.ArrayList;
import java.util.List;

public class OrderFrom2OrderDTO {
    public static OrderDTO convert(OrderForm orderForm)
    {
        Gson gson = new Gson();
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName(orderForm.getName());
        orderDTO.setBuyerPhone(orderForm.getPhone());
        orderDTO.setBuyerAddress(orderForm.getAddress());
        orderDTO.setBuyerOpenid(orderForm.getOpenid());

        List<OrderDetail> orderDetailList = new ArrayList<>();
        try{
            orderDetailList = gson.fromJson(orderForm.getItems(),
                    new TypeToken<List<OrderDetail>>(){}.getType());
        }catch (Exception e){
            throw new SellException(ResultEnum.PARAM_ERROR);
        }
        orderDTO.setOrderDetailList(orderDetailList);
        return orderDTO;
    }
}
