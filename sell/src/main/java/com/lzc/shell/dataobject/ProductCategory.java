package com.lzc.shell.dataobject;

/*
    类目表
 */

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@DynamicUpdate
@Data
public class ProductCategory {
    /*类目id*/
    @Id
    @GeneratedValue
    private Integer categoryId;
    /*类目类型*/
    private String categoryName;
    /*类目类型*/
    private Integer categoryType;

    private Date createTime;

    private Date updateTime;

    public ProductCategory() { }

    public ProductCategory(String categoryName, Integer categoryType) {
        this.categoryName = categoryName;
        this.categoryType = categoryType;
    }
}
