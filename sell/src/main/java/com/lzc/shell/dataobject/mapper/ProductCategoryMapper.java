package com.lzc.shell.dataobject.mapper;

import com.lzc.shell.dataobject.ProductCategory;
import org.apache.ibatis.annotations.*;

import java.util.Map;
@Mapper
public interface ProductCategoryMapper {

    @Select("select * from product_category where category_id = #{category_id}")
    @Results(
            {
                    @Result(property = "categoryName",column = "category_name"),
                    @Result(property = "categoryType",column = "category_type")
            })
    ProductCategory findById(@Param("category_id")Integer category_id);

}
