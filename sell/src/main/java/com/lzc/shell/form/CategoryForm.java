package com.lzc.shell.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryForm {
    private Integer categoryId;
    @NotNull(message = "类目名字不能为空")
    /** 类目名字. */
    private String categoryName;
    @NotNull(message = "类目编号不能为空")
    /** 类目编号. */
    private Integer categoryType;
}
