package com.lzc.shell.handler;

import com.lzc.shell.exception.SellException;
import com.lzc.shell.exception.SellerAuthorizeException;
import com.lzc.shell.util.ResultVOUtil;
import com.lzc.shell.vo.ResultVo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class SellExceptionHandler {

    //拦截登陆异常
    @ExceptionHandler(value = SellerAuthorizeException.class)
    public ModelAndView handlerAuthorizeException()
    {
        return new ModelAndView("redirect:"+"http://localhost:8080/sell/seller/login");
    }

    @ExceptionHandler(value = SellException.class)
    @ResponseBody
    public ResultVo handlerSellException(SellException e)
    {
        return ResultVOUtil.error(e.getCode(),e.getMessage());
    }
}
