package com.lzc.shell.repository;

import com.lzc.shell.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductInfoRepository extends JpaRepository<ProductInfo,String>{
    /*

     */
    List<ProductInfo> findByProductStatus(Integer productStatus);
}
