package com.lzc.shell.service;

import com.lzc.shell.dto.OrderDTO;

public interface PayService {

    void create(OrderDTO orderDTO);
}
