package com.lzc.shell.service;
/*
    推送消息
 */

import com.lzc.shell.dto.OrderDTO;

public interface PushMessage {

    /*
        d订单状态变更消息
     */
    void orderStatus(OrderDTO orderDTO);
}
