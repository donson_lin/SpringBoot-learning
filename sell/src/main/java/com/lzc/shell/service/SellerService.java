package com.lzc.shell.service;

import com.lzc.shell.dataobject.SellerInfo;

public interface SellerService {

    /*
        根据openid查询卖家端信息
     */
    SellerInfo findSellerInfoByOpenid(String openid);
}
