package com.lzc.shell.service.impl;

import com.lzc.shell.dto.OrderDTO;
import com.lzc.shell.service.PushMessage;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class PushMessageImpl implements PushMessage {

    @Autowired
    private WxMpService wxMpService;

    @Override
    public void orderStatus(OrderDTO orderDTO) {
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser("orJtH1KxYupQROh0_B8JDo4FS1L8")
                .templateId("vn9XCiC-SZgun4YKzL1iCFySLp-krOeirpWMV86uM5o")
                .url("").build();
        templateMessage.getData().add(new WxMpTemplateData("first", "亲！记得收货"));
        templateMessage.getData().add(new WxMpTemplateData("keyword1", "微信点餐"));
        templateMessage.getData().add(new WxMpTemplateData("keyword2", "18812345678"));
        templateMessage.getData().add(new WxMpTemplateData("keyword3", orderDTO.getOrderId()));
        templateMessage.getData().add(new WxMpTemplateData("keyword4", orderDTO.getOrderStatusEnum().getMessage()));
        templateMessage.getData().add(new WxMpTemplateData("keyword5", "¥:"+orderDTO.getOrderAmount()+""));
        templateMessage.getData().add(new WxMpTemplateData("remark", "欢迎再次购买"));
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
        }catch (WxErrorException e){
            log.info("微信模板发送失败");
        }


    }
}
