package com.lzc.shell.service.impl;

import com.lzc.shell.dataobject.SellerInfo;
import com.lzc.shell.repository.SellerInfoRepository;
import com.lzc.shell.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerServiceImpl implements SellerService {

    @Autowired
    private SellerInfoRepository repository;
    @Override
    public SellerInfo findSellerInfoByOpenid(String openid) {
        return repository.findByOpenid(openid);
    }
}
