package com.lzc.shell.util;

import com.lzc.shell.vo.ResultVo;

public class ResultVOUtil {

    public static ResultVo success(Object object)
    {
        ResultVo resultVo = new ResultVo();
        resultVo.setCode(0);
        resultVo.setMessage("成功");
        resultVo.setData(object);
        return resultVo;
    }

    public static ResultVo success()
    {
        return success(null);
    }

    public static ResultVo error(Integer code,String message)
    {
        ResultVo resultVo = new ResultVo();
        resultVo.setCode(code);
        resultVo.setMessage(message);
        resultVo.setData(null);
        return resultVo;
    }
}
