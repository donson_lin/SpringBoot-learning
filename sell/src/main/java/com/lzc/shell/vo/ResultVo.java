package com.lzc.shell.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultVo <T> implements Serializable{

    private static final long serialVersionUID = 3591322194914604796L;

    /*错误码*/
    private Integer code;
    /*提示信息*/
    private String message;
    /*具体内容*/
    private T data;
}
