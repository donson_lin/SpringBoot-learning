<html>
<#include "common/header.ftl">

<body>
<div id="wrapper" class="toggled">
    <#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <form class="form-signin" action="/sell/seller/dologin" method="post">
                        <h2 class="form-signin-heading">Please sign in</h2>
                        <label for="inputEmail" class="sr-only">请输入账号</label>
                        <input type="text" id="inputEmail" name="openid" class="form-control" placeholder="请输入账号" required autofocus>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>