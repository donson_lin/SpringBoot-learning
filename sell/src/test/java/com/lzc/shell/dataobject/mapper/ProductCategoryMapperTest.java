package com.lzc.shell.dataobject.mapper;

import com.lzc.shell.dataobject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryMapperTest {

    @Autowired
    private ProductCategoryMapper mapper;

    @Test
    public void insertByMapper() throws Exception{

        ProductCategory productCategory = mapper.findById(1);
        System.out.println(productCategory.getCategoryName());
        Assert.assertNotNull(productCategory);
    }
}