package com.lzc.shell.repository;

import com.lzc.shell.dataobject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryRepositoryTest {
    @Autowired
    private ProductCategoryRepository repository;
    @Test
    public void findOne()
    {
        ProductCategory productCategory = repository.findOne(1);
        System.out.println(productCategory.toString());
    }

    @Test
    public void save()
    {
        ProductCategory productCategory = new ProductCategory("女生不喜欢",1);
        ProductCategory result = repository.save(productCategory);
        Assert.assertNotNull(result);
        System.out.println(result.toString());
    }


    @Test
    public void findByCategoryTypeIn()
    {
        List<Integer> list = Arrays.asList(1,2,3,4,5);
        List<ProductCategory> results = repository.findByCategoryTypeIn(list);
        for(ProductCategory x:results)
        {
            System.out.println(x.toString());
        }
    }

}