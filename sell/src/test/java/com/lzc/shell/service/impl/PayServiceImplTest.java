package com.lzc.shell.service.impl;

import com.lzc.shell.dto.OrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PayServiceImplTest {

    @Autowired
    private PayServiceImpl payService;

    @Autowired
    private OrderServiceImpl orderService;
    @Test
    public void create() {

        //幼师wxaccd1000baf1673a
        //我的wx7026a4548c27b218
        payService.create(orderService.findOne("3213651519279997669"));
    }
}