package com.lzc.shell.service.impl;

import com.lzc.shell.dto.OrderDTO;
import com.lzc.shell.service.OrderService;
import com.lzc.shell.service.PushMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PushMessageImplTest {

    @Autowired
    private PushMessage pushMessage;

    @Autowired
    private OrderService orderService;

    @Test
    public void orderStatus() {
        OrderDTO orderDTO = orderService.findOne("1194201518486446970");
        pushMessage.orderStatus(orderDTO);
    }
}