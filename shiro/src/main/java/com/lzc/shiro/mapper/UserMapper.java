package com.lzc.shiro.mapper;

import com.lzc.shiro.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    public User findByUsername(@Param("username") String username);
}
