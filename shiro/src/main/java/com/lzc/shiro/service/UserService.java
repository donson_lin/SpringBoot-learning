package com.lzc.shiro.service;

import com.lzc.shiro.model.User;

public interface UserService {
    User findByUsername(String username);
}
