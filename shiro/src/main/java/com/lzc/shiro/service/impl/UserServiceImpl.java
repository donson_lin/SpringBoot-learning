package com.lzc.shiro.service.impl;


import com.lzc.shiro.mapper.UserMapper;
import com.lzc.shiro.model.User;
import com.lzc.shiro.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }
}
